var images  = ['/img/stud1.png', '/img/stud2.png', '/img/stud3.png', '/img/stud4.png'];
$target = $('#background');
i = 0;

setInterval(function() {
    $target.animate({ opacity: 0 }, 500, function() {
        $target.css('background-image', 'url('+images[++i]+')');
        $target.animate({ opacity: 1 }, 500, function() {
            if(i === images.length) i = 0;
        });
    });
 }, 6000);